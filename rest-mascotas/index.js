const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const port = 8090;
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

var mascotas = [
  {
    id: 1,
    nombre: "pepe trueno",
    tipo: "gato",
    edad: 3,
    descripcion: "gruñon y juguetón",
    img: "https://estaticos.miarevista.es/media/cache/760x570_thumb/uploads/images/article/57725157a1d4251a098bc9a3/ppal-ronroneogato.jpg"
  },
  {
    id: 2,
    nombre: "chispita",
    tipo: "perro",
    edad: 5,
    descripcion: "se comporta muy bien",
    img: "https://www.eluniversal.com.mx/sites/default/files/styles/f03-651x400/public/2019/06/20/perro_comunicar_mascota_0.jpg?itok=BCp_XA7c"
  },
  {
    id: 3,
    nombre: "rafael",
    tipo: "tortuga",
    edad: 2,
    descripcion: "le encanta la lechuga y pasear por la casa (si, también le gusta la pizza)",
    img: "https://estaticos.muyinteresante.es/media/cache/760x570_thumb/uploads/images/article/5d512e285cafe843661aca56/tortuga-verde-chelonia-mydas.jpg"

  },
  {
    id: 4,
    nombre: "yogui",
    tipo: "perro",
    edad: 1,
    descripcion: "super activo, le gusta correr mucho",
    img: "https://animalesmascotas.com/wp-content/uploads/2015/05/TIPOS-DE-BULLDOG-FRANCES-INGLES-Y-AMERICANO.jpg"
  },
  {
    id: 5,
    nombre: "piolin",
    tipo: "guacamaya",
    edad: 3,
    descripcion: "muy colorida, come frutas y ¡habla!",
    img: "https://cdne.diariocorreo.pe/thumbs/uploads/img/2019/03/16/nace-guacamayo-azul-especie-que-se-consideraba-876219-056253-jpg_604x0.jpg"
  }
];

app.post("/mascotas", function(req, res) {
  let mascota = req.body;
  let ids = mascotas.map(elt => elt.id);
  if (mascotas.length == 0) {
    mascota.id = 1;
  } else {
    mascota.id = Math.max(...ids) + 1;
  }
  mascotas.push(mascota);
  res.status(201).json(mascota);
});

app.get("/mascotas", function(req, res) {
  res.status(200).json(mascotas);
});

app.get("/mascotas/:id", function(req, res) {
  res.status(200).json(mascotas.find(elt => elt.id == req.params.id));
});

app.put("/mascotas", function(req, res) {
  let index = mascotas.findIndex(elt => elt.id == req.body.id);
  if (index >= 0) mascotas[index] = req.body;
  res.status(200).send();
});

app.delete("/mascotas/:id", function(req, res) {
  let index = mascotas.findIndex(elt => elt.id == req.params.id);
  if (index >= 0) mascotas.splice(index, 1);
  res.status(200).send();
});

app.listen(port, () => {
  console.log("El servidor está inicializado en el puerto " + port);
});
