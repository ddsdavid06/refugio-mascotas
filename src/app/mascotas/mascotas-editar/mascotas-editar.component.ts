import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import { MascotasService } from "../shared/mascotas.service";
import { Mascota } from "../shared/mascota";

@Component({
  selector: "app-mascotas-editar",
  templateUrl: "./mascotas-editar.component.html",
  styleUrls: ["./mascotas-editar.component.css"]
})
export class MascotasEditarComponent implements OnInit {
  mascotaForm = this.fb.group({
    id: [""],
    nombre: ["", [Validators.required]],
    tipo: ["", [Validators.required]],
    edad: ["", [Validators.required, Validators.pattern("[1-9]+")]],
    descripcion: ["", [Validators.required]],
    img: ["", [Validators.required]]
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: MascotasService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get("id");

    this.service.getMascota(Number(id)).subscribe(data => {
      this.mascotaForm.setValue(data);
    });
  }

  submit() {
    this.service.updateMascota(this.mascotaForm.value).subscribe(response => {
      this.router.navigate(["/mascotas-listar"]);
    });
  }

  backMascotasListar($event) {
    $event.preventDefault();
    this.router.navigate(["/mascotas-listar"]);
  }
}
