import { Component, OnInit, Input } from '@angular/core';
import { Mascota } from '../shared/mascota';
import { MascotasService } from '../shared/mascotas.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-mascota-card',
  templateUrl: './mascota-card.component.html',
  styleUrls: ['./mascota-card.component.css']
})
export class MascotaCardComponent implements OnInit {
 mascota:Mascota;

  constructor(
    private route: ActivatedRoute,
    private service: MascotasService) { }

  ngOnInit() {

    let id = this.route.snapshot.paramMap.get('id');

    this.service.getMascota(Number(id)).subscribe(data => {
      this.mascota = data;
  });
  }
}
