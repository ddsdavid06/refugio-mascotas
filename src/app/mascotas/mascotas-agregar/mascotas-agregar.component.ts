import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MascotasService } from '../shared/mascotas.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-mascotas-agregar',
  templateUrl: './mascotas-agregar.component.html',
  styleUrls: ['./mascotas-agregar.component.css']
})
export class MascotasAgregarComponent implements OnInit {

  mascotasForm = new FormGroup({
    nombre: new FormControl('',[Validators.required]),
    tipo: new FormControl('',[Validators.required]),
    edad: new FormControl('',[Validators.required, Validators.pattern("[1-9]+")]),
    descripcion: new FormControl('',[Validators.required]),
    img: new FormControl('',[Validators.required])
  });

  constructor(private mascotasService: MascotasService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(){
    this.mascotasService.addMascota(this.mascotasForm.value).subscribe(response => {
      this.router.navigate(['/mascotas-listar']);
    });
  }

  reset($event){
    $event.preventDefault();
    this.mascotasForm.reset();
  }

  backMascotasListar($event){
    $event.preventDefault();
    this.router.navigate(['/mascotas-listar']);
  }
}
